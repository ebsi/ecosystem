# EBSI-DEP ecosystem

## EBSI Improvement Proposals (EBIPs)

A EBSI Improvement Proposal (or EBIP, pronounced "e-bip") is a document which offers ways to improve EBSI ecosystem via a standard defining consistent behaviour or interaction of multiple products (e.g. smart contract specification).

## What is a EBIP?

A EBIP is a design document providing information to the EBSI community,
describing interoperability standard. The EBIP process complements (but is
subsidiary to) EBSI' formal on-chain governance process.

A EBIP should contain a concise technical specification and rationale which
unambiguously articulates what the proposal entails, how it may be implemented,
and why the proposal improves on the status quo.

A EBIP begins as a **EBIP draft**. Once adopted, a EBIP draft becomes a numbered
EBIP-### (e.g. EBIP-010).

## How to Contribute

There are mostly 2 main ways to contribute:

1. review existing draft EBIPs (see the table below) and comment, edit...
2. propose you own draft EBIP explaining the problem you would like to solve and idea of solution in
this repository.

- Make sure the draft contains a concise technical specification and rationale.
- Clearly articulate what the proposal entails, how it can be implemented, and why it's an improvement.
- Follow the naming conventions and formatting guidelines specified in the [EBIP draft template].
- Increment the version number and date of the draft for any substantial changes.

Open a merge request using the [EBIP draft template]. An overview of EBIP drafts
can be found in [drafts README].

[EBIP draft template]: https://gitlab.com/EBSI/EBIP/-/blob/master/templates/EBIP-template.md
[drafts README]: https://gitlab.com/EBSI/EBIP/-/blob/master/drafts/README.md

## Approved/Active EBIPs

| EBIP  | Title | Creation Date | Status |
| :---: | :---- | :-----------: | :----- |

## Draft/WIP EBIPs (open for feedback)

|                                                            EBIP                                                             | Title                           | Creation Date | Status |
| :-------------------------------------------------------------------------------------------------------------------------: | :------------------------------ | :-----------: | :----- |
|                 [EBIP-001](https://code.europa.eu/ebsi/ecosystem/-/blob/EBIP-001/drafts/EBIP-001.md)                 | EBIP validation process    |  2023-06-12   | draft  |
|                 [EBIP-link](https://code.europa.eu/ebsi/ecosystem/-/blob/EBIP-SD-JWT/drafts/draft-sd-jws.md)                 | Selective Disclosure for JWS    |  2023-06-29   | draft  |
| [EBIP-link](https://code.europa.eu/ebsi/ecosystem/-/blob/ebip/oid4vp-presentation-offer/drafts/oid4vp-presentation-offer.md) | OID4VP Presentation Offer       |  2023-09-04   | draft  |
|               [EBIP-link](https://code.europa.eu/ebsi/ecosystem/-/blob/ebip/vc-login/drafts/draft-vc-login.md)               | Universal Login                 |  2023-09-14   | draft  |
|           [EBIP-link](https://code.europa.eu/ebsi/ecosystem/-/blob/ebip/key-wallet-attestation/drafts/EBIP-007.md)           | Key and Wallet/App attestations |  2023-09-19   | draft  |
|           [EBIP-link](https://code.europa.eu/ebsi/ecosystem/-/blob/ebip/vc-presentation-request-policies/drafts/vc-request-present-policies.md)           | VC Request and Presentation policies |  2023-08-25   | draft  |
|           [EBIP-link](https://code.europa.eu/ebsi/ecosystem/-/blob/ebip/issuer-verifier-authentication/drafts/issuer-verifier-authentication.md)           | Issuer and Verifier identification and authentication |  2023-09-06   | draft  |
