---
title: EBIP Title - a short descriptive title (max 44 characters)
status: Draft - will remain "Draft" until accepted
author: Name(s) of author(s), ideally with their username(s) or email address(es)
type: EBIP type (TBD)
created: Date the EBIP was created, format yyyy-mm-dd
requires: Optional. EBIP numbers, representing EBIPs that this EBIP depends on
replaces: Optional. EBIP numbers, representing the EBIPs the current EBIP is
replacing
superseded-by: Optional. EBIP numbers, representing the EBIPs replacing the
current EBIP
date: for drafts, the date on which the draft was last
changed, in yyyy-mm-dd format
version: for drafts, a version number, starting from 0, incremented whenever
a substantive change occurs warranting re-review.
---


## Summary

Provide a simplified and layman-accessible explanation of the EBIP.

## Abstract

A short (200-500 word) but comprehensive description of the issue being
addressed and the proposed solution.

## Motivation

It should clearly explain why the existing implementation is inadequate to
address the problem that the EBIP solves.

## Specification

The technical specification should describe the syntax and semantics of any new
feature.

## Rationale

The rationale fleshes out the specification by describing what motivated the
design and why particular design decisions were made. It should describe
alternate designs that were considered and related work. The rationale may also 
provide evidence of consensus within the community, and should discuss important 
objections or concerns raised during discussion.

## Backwards Compatibility

All EBIPs that introduce backwards incompatibilities or supersede other EBIPs
must include a section describing these incompatibilities, their severity, and
solutions.

## Security Considerations

This section of the document should explain any security relevant features
of the proposal, or any critical issues for implemenenters. 

## Test Cases

Test cases for an implementation are recommended as are proofs of correctness via 
formal methods if applicable.

## Implementations

Any code already written.

## Appendix

A list of references relevant to the proposal.

## Copyright

All EBIPs must be in the public domain, or a under a permissive license
substantially identical to placement in the public domain. 


Example of a copyright waiver:

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
