---
EBIP: 002
title: EBIP Types and Naming
status: Final
type: Meta
author: Wes
created: 2023-03-08
---

## Summary

This document lists the different types a EBIP can be, with their descriptions.
In addition, we explain how to use types to name some EBIPs.


## List of EBIP Types

| Type Prefix | Topic                  | Description                |
|-------------|------------------------|----------------------------|
| `A`         | Application            | Applications built on top of EBSI, particularly smart contract or higher layer applications. |
| `LA`        | Layer-n Application    | Higher-layer applications which use the EBSI chain e.g. for settlement or communication. |
| `I`         | Interface              | Improvements around client API/RPC specifications and standards. |
| `L`         | Language               | Improvements to smart contracts, APIs or best practices for EBSI projects using JavaScript, etc. |
| -           | Meta                   | This type describes a process surrounding EBSI or proposes a change to (or an event in) a process. Examples include procedures, guidelines, changes to the EBIP process itself, or proposals for new social institutions and spaces. The present document is a Meta EBIP. |
| -           | Informational          | Discusses a EBSI design issue, or provides general guidelines or information to the EBSI community. |

Additional types of EBIP may be proposed, and existing EBIP types may also be
deprecated by future Meta EBIPs. All the updates will be maintained in this EBIP
([EBIP-002](/proposals/EBIP-2/EBIP-2.md)).


## Naming

Some EBIPs, like smart contract specifications, have a distinct *name* that is
used to identify them. This is in addition to their EBIP number.

We define the EBIP name as two components:

1. An alphabetic Type prefix (e.g. FA or P) which signifies the standard's topic
or domain of relevance. The valid EBIP types are available in the *List of
Types* discussed above.
2. A serial number represented as a list of dot separated numbers (e.g. 1.2.3 or
15), which signifies whether the standard is an extension or specialization of
another more general standard. For example 2.34 would be the extension number 34
of the root standard number 2.


## Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
